#ifndef PHONEEDIT_H
#define PHONEEDIT_H

#include <QLineEdit>
#include <QToolButton>
#include <QPainter>
#include <QTableWidget>
#include <QListWidget>
#include "countrytable.h"

class PhoneEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit PhoneEdit(QWidget *parent = 0);
    explicit PhoneEdit(const QString&, QWidget* parent=0);

    void resizeEvent(QResizeEvent *);
    void paintEvent(QPaintEvent *);
signals:

public slots:
    void ChooseCountry ();
    void RowClicked(int row, int col);
    void TextChanged (const QString &);

private:
    QToolButton* prefixChooser;
    QTableWidget* countryTab;

    int countryNum;
    int flagNum;

    void CreateCountryTable(QWidget *parent);
};

#endif // PHONEEDIT_H
