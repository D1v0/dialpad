#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include "phoneedit.h"
#include "smsfield.h"
#include "label.h"

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();
    void paintEvent (QPaintEvent *);

private:
    int width_;
    int height_;

    QLabel* creditLabel;
    QLabel* credit;
    QLabel* buyCreditLabel;
    PhoneEdit* phoneNumber;

    QPushButton* send;

    SmsField* smsField;
    Label* charCount;

    QLabel* call;
    QLabel* seeRates;
};

#endif // WIDGET_H
