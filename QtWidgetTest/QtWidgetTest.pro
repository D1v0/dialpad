#-------------------------------------------------
#
# Project created by QtCreator 2015-06-11T23:33:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Dialpad
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    label.cpp \
    phoneedit.cpp \
    countrytable.cpp \
    smsfield.cpp

HEADERS  += widget.h \
    label.h \
    phoneedit.h \
    countrytable.h \
    smsfield.h

RESOURCES += \
    res.qrc
