#include "label.h"

Label::Label(QWidget *parent) :
    QLabel(parent)
{
}

Label::Label(const QString &text, QWidget *parent, Qt::WindowFlags f):
    QLabel(text, parent, f)
{
    this->setObjectName("List");
    this->setStyleSheet("#List {font-size: 13px; color: blue}");
}

void Label::SetLine (const QString &part1, const QString &part2)
{
    QString richText("<html><head><meta name=\"qrichtext\" content=\"1\" /></head>"
                     "<body>"
                     "<p style=\"font-family:MS Shell Dlg 2;font-size:11pt;\">"
                     + part1 +
                     "<span style=\"color: green\">"
                     + "/" + part2 +
                     "</span>"
                     "</p>"
                     "</body>"
                     "</html>");
   this->setText(richText);
}
