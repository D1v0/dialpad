#include "phoneedit.h"
#include <QHeaderView>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

PhoneEdit::PhoneEdit(QWidget *parent) :
    QLineEdit(parent)
{
    this->flagNum = -1;

    this->prefixChooser = new QToolButton (this);
    this->prefixChooser->setCursor(Qt::ArrowCursor);
    this->prefixChooser->setStyleSheet("QToolButton {border: none; padding: 0px; image: url(:/bt/arrow); width: 10px}");

    this->setObjectName("PhoneEdit");

    this->setStyleSheet("#PhoneEdit {height: 30px; border-image: url(:/fl/field_normal); padding-left: 40px;} #PhoneEdit:hover {height: 30px; border-image: url(:/fl/field_focus); padding-left: 40px;}");
    this->setPlaceholderText("Enter phone number");

    QObject::connect (this->prefixChooser, SIGNAL (clicked()), this, SLOT (ChooseCountry()));
    QObject::connect (this, SIGNAL (textChanged(const QString &)), this, SLOT (TextChanged(const QString &)));

    this->CreateCountryTable (parent);
}

PhoneEdit::PhoneEdit(const QString &s, QWidget* parent):
    QLineEdit(s, parent)
{

}

void PhoneEdit::resizeEvent(QResizeEvent *)
{
    QSize sz = this->prefixChooser->sizeHint();
    this->prefixChooser->move(rect().left() + 27, (rect().bottom() + 1 - sz.height()) / 2);
}

void PhoneEdit::paintEvent(QPaintEvent *p)
{
    QLineEdit::paintEvent(p);

    QPainter painter (this);

    if (this->flagNum != -1)
    {
        QString fileName = QString(":/au/%1").arg(this->flagNum);

        QFile flagImage (fileName);
        if (flagImage.open(QIODevice::ReadOnly))
        {
            flagImage.close();
            QImage image (fileName);
            painter.drawImage(QPoint (7, this->height() / 2 - image.height() / 2), image);
        }
    }
}

void PhoneEdit::ChooseCountry ()
{
    this->countryTab->raise();
    this->countryTab->move(QPoint (0, 40));
    this->countryTab->show();
}

void PhoneEdit::RowClicked(int row, int col)
{
    this->flagNum = this->countryTab->item(row, 3)->text().toInt();
    this->setText(this->countryTab->item(row, 2)->text().trimmed());

    this->countryTab->hide();
    this->update();
    this->setFocus();
}

void PhoneEdit::TextChanged (const QString & str)
{
    if ((str.length() == 0) || ((str.length() == 1) && (str == "+")))
        this->flagNum = -1;

    QString prefix ("+");

    if (this->text().left(1) == "+")
    {
        prefix = str;
    }
    else
    {
        prefix.append(str);
    }

    QList <QTableWidgetItem*> tmp = this->countryTab->findItems(prefix, Qt::MatchFixedString);
    if (tmp.length() == 0)
        return;
    QTableWidgetItem* item = this->countryTab->item(tmp.at(0)->row(), 2);
    item = this->countryTab->item(item->row(), 3);

    this->flagNum  = item->text().toInt();
    this->update();
}

void PhoneEdit::CreateCountryTable(QWidget *parent)
{
    QFile countryFile (":/au/countries");
    countryFile.open(QIODevice::ReadOnly);
    QTextStream in(&countryFile);

    this->countryTab = new QTableWidget (0, 4, parent);
    this->countryTab->hide();
    this->countryTab->setSelectionBehavior(QAbstractItemView::SelectRows);
    connect (this->countryTab, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(RowClicked(int, int)));

    this->countryTab->setGeometry(0, 0, 300, 200);
    this->countryTab->horizontalHeader()->hide();
    this->countryTab->verticalHeader()->hide();
    this->countryTab->setFrameShadow(QFrame::Plain);
    this->countryTab->setColumnWidth(0, 25);
    this->countryTab->setColumnWidth(2, 150);
    this->countryTab->hideColumn(3);

    int row = 0;

    while(!in.atEnd())
    {
        this->countryTab->insertRow(this->countryTab->rowCount());

        QString line = in.readLine();
        QStringList fields = line.split(",");

        QTableWidgetItem* item0 = new QTableWidgetItem;
        item0->setIcon(QIcon (QPixmap(QString(":/au/%1").arg(fields.at (0).trimmed()))));
        this->countryTab->setItem(row, 0, item0);

        QTableWidgetItem* item1 = new QTableWidgetItem;
        item1->setText(fields.at (2)); // вставляем текст
        this->countryTab->setItem(row, 1, item1);

        QTableWidgetItem* item2 = new QTableWidgetItem;
        item2->setText(QString("+%1").arg(fields.at (1).trimmed()));
        item2->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        this->countryTab->setItem(row, 2, item2);

        QTableWidgetItem* item3 = new QTableWidgetItem;
        item3->setText(fields.at (0).trimmed());
        this->countryTab->setItem(row, 3, item3);

        ++row;
    }
    this->countryTab->sortByColumn(2, Qt::AscendingOrder);
}
