#include "widget.h"

#include <QApplication>
#include <QDesktopWidget>

#include <QStyleOption>
#include <QPainter>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QTextEdit>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QAction>

Widget::Widget(QWidget *parent)
    : QWidget(parent), width_ (302), height_ (309)
{
    this->setWindowFlags (windowFlags () | Qt::WindowMaximizeButtonHint
                | Qt::MSWindowsFixedSizeDialogHint);

    this->setObjectName("MainWidget");

    this->setGeometry(0, 0, this->width_, this->height_);
    this->setMinimumSize(this->width_, this->height_);
    this->move(QApplication::desktop()->screen()->rect().center() - this->rect().center());

    this->setStyleSheet("#MainWidget {background-image: url(:/bg/roster);border-image: url(:/bg/background)  0 330 10 0; background-position: bottom left;}");

    this->creditLabel = new QLabel ("Credit: ");
    this->creditLabel->setObjectName("creditLabel");
    this->creditLabel->setStyleSheet("#creditLabel {font-size: 13px;}");

    this->credit = new QLabel ("$0.00");
    this->credit->setObjectName("credit");
    this->credit->setStyleSheet("#credit {font-size: 13px; color: #7159C9}");

    this->buyCreditLabel = new QLabel ("Buy credit");
    this->buyCreditLabel->setObjectName("buyCreditLabel");
    this->buyCreditLabel->setStyleSheet("#buyCreditLabel {font-size: 13px; color: #3B5EC1}");

    this->phoneNumber = new PhoneEdit (this);

    this->send = new QPushButton ("Send");
    this->send->setObjectName("send");
    this->send->setStyleSheet("#send {font-size: 13px; color: white; width: 40px; height: 30px; border-image: url(:/bt/btn_normal);} #send:hover {font-size: 13px; color: white; width 40px; height: 30px; border-image: url(:/bt/btn_hover);} #send:pressed {font-size: 13px; color: white; width 40px; height: 30px; border-image: url(:/bt/btn_down);}");

    QVBoxLayout* mainLayout = new QVBoxLayout;

    QGridLayout* firstRow = new QGridLayout;
    firstRow->addWidget(this->creditLabel, 0, 0, Qt::AlignLeft | Qt::AlignTop);
    firstRow->addWidget(this->credit, 0, 1, 1, 10, Qt::AlignLeft | Qt::AlignTop);
    firstRow->addWidget(this->buyCreditLabel, 0, 11, Qt::AlignRight | Qt::AlignTop);
    mainLayout->addLayout(firstRow);

    QGridLayout* l2 = new QGridLayout;
    l2->addWidget(this->phoneNumber, 0, 0, 2, 9);
    l2->addWidget(this->send, 0, 10, 2, 1, Qt::AlignRight);
    mainLayout->addLayout(l2);

   this->smsField = new SmsField;
    mainLayout->addWidget(smsField);

    QGridLayout* l3 = new QGridLayout;
    this->charCount = new Label;
    this->smsField->SetSmsCountWidget(this->charCount);
    l3->addWidget(charCount, 0, 1, Qt::AlignRight);
    mainLayout->addLayout(l3);

    QGridLayout* l4 = new QGridLayout;
    this->call = new QLabel ("Call");
    this->call->setObjectName("call");
    this->call->setStyleSheet("#call {font-size: 13px; color: black}");
    l4->addWidget(this->call, 0, 0, Qt::AlignLeft);

    this->seeRates = new QLabel ("See rates");
    this->seeRates->setObjectName("see");
    this->seeRates->setStyleSheet("#see {font-size: 13px; color: #025EA1}");
    l4->addWidget(this->seeRates, 0, 1, 1, 2, Qt::AlignRight);
    mainLayout->addLayout(l4);

    this->setLayout(mainLayout);
}

Widget::~Widget()
{

}

void Widget::paintEvent (QPaintEvent *)
{
    QStyleOption opt;
    opt.init (this);
    QPainter p (this);
    style ()->drawPrimitive (QStyle::PE_Widget, &opt, &p, this);
}
