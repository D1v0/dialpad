#ifndef SMSFIELD_H
#define SMSFIELD_H

#include <QTextEdit>
#include <label.h>

class SmsField : public QTextEdit
{
    Q_OBJECT
public:
    explicit SmsField(QWidget *parent = 0);
    void SetSmsCountWidget (Label* l);

signals:

public slots:
    void TextChanged ();

private:
    bool clicked;
    Label* smsCount;

    virtual void focusInEvent(QFocusEvent* e);
    virtual void focusOutEvent(QFocusEvent* e);
};

#endif // SMSFIELD_H
