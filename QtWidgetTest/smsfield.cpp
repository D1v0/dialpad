#include "smsfield.h"
#include <QMessageBox>

SmsField::SmsField(QWidget *parent) :
    QTextEdit(parent)
{
    this->setObjectName("CountryTab");
    this->setStyleSheet("#CountryTab {border-image: url(:/fl/field_normal) 2 2 10 2; border: 2px solid; color: #DBC2B5; font-size: 13px}");
    this->setText("Type message");

    connect(this, SIGNAL(textChanged()), this, SLOT(TextChanged()));

     this->clicked = false;
}

void SmsField::focusInEvent(QFocusEvent* e)
{
    if (!this->clicked)
    {
        this->setStyleSheet("#CountryTab {border-image: url(:/fl/field_normal) 2 2 10 2; border: 2px solid; color: black; font-size: 13px}");
        this->clear();
        this->clicked = true;
    }
    QTextEdit::focusInEvent(e);
}

void SmsField::focusOutEvent(QFocusEvent* e)
{
    if (this->clicked)
    {
        if (this->toPlainText().length() == 0)
        {
           this->setStyleSheet("#CountryTab {border-image: url(:/fl/field_normal) 2 2 10 2; border: 2px solid; color: #DBC2B5; font-size: 13px}");
            this->setText("Type message");
            this->clicked = false;
        }
    }
    QTextEdit::focusInEvent(e);
}

void SmsField::SetSmsCountWidget (Label* l)
{
    this->smsCount = l;
    this->smsCount->SetLine("160", "1");
}

void SmsField::TextChanged ()
{
    int count = 1;
    if (this->toPlainText().count() > 0)
    {
        count += (this->toPlainText().count()) / 160;
    }

    int scount = 160 * count - this->toPlainText().count();
    this->smsCount->SetLine(QString::number(scount), QString::number(count));
}
