#ifndef LABEL_H
#define LABEL_H

#include <QLabel>

class Label : public QLabel
{
    Q_OBJECT
public:
    explicit Label(QWidget *parent = 0);
    explicit Label(const QString &text, QWidget *parent=0, Qt::WindowFlags f=0);

signals:

public slots:

public:
    void SetLine (const QString &part1, const QString &part2);
};

#endif // LABEL_H
